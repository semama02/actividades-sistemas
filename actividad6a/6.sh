read -p "Dime la cantidad de litros " valor1

while [ $valor1 -le 0 ] ;do
	read -p "Dime la cantidad de litros" valor1
done

coste=0

if [ $valor1 -le 50 ]
then
	coste=20
else
	if [ $valor1 -le 200 ]
	then
		pendiente=$((valor1-50))
		coste=`echo "scale=2; $pendiente*0.2+20" | bc`
	else
		pendiente=$((valor1-200))
		coste=`echo "scale=2; $pendiente*0.1+20+(150*0.2)" | bc`
	fi
fi
echo "El coste de $valor1 es $coste"

