clientelinux=0;
clientewindows=0;
procesoslinux=0;
procesoswindows=0;
#9
 
max=`cat listado.txt | wc -l`
 
contador=1
 
while [ $contador -ne $((max+1)) ];do
 
	p=`cat listado.txt | head -${contador} | tail -1 | awk '{print $2}'`;
	s=`cat listado.txt | head -${contador} | tail -1 | awk '{print $3}'`;
 
 
	if [ $p = "Linux" ];then
		clientelinux=$((clienteslinux+1))
		procesoslinux=$((procesoslinux+s))
	else
		clienteswindows=$((clienteswindows+1))
		procesoswindows=$((procesoswindows+s))
	fi
 
	contador=$((contador+1));
done
 
echo "Linux -> $clientelinux $procesoslinux"
echo "Windows -> $clienteswindows $procesoswindows"
